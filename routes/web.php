<?php

use App\Mail\ContactForm;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/about-us', function () {
    return view('about-us');
});

Route::get('/programms', function () {
    return view('program');
});

Route::get('/contact-us', function () {
    return view('contact-us');
});

Route::post('/contact-form', function (Request $request) {

	Mail::to('admin123@yopmail.com')->send(new ContactForm($request->all()));

	return redirect()->back();
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/print-certificate', 'HomeController@PrintCertificate')->name('print-certificate');

