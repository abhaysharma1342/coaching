<?php

namespace App\Http\Controllers;

use PDF;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function PrintCertificate(Request $request)
    {
        $data = ['title'    => $request->name, 
                 'heading'  => $request->course .' Certificatation.',            
                 'content'  => 'This is certified that <b>'.$request->name.'</b> Has Successfully Completed the <b>'.$request->course .'</b> Certificatation Program of duration <b>'.$request->duration.' '.$request->duration_type.'</b>.'        
                ];
        
        $pdf = PDF::loadView('certificate', $data);  
        return $pdf->download($request->name.'.pdf');
    }
}
