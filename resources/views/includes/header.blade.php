<header class="header_section">
    <div class="container">
        <nav class="navbar navbar-expand-lg custom_nav-container ">
            <a class="navbar-brand" href="{{ Request::is('/') ? '' : '/' }}">
                <img src="images/logo.png" alt="" />
                <span>
                  MSIICT
                </span>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <div class="d-flex ml-auto flex-column flex-lg-row align-items-center">
                    <ul class="navbar-nav">

                        <li class="{{ Request::is('/') ? 'nav-item active' : 'nav-item' }}">
                          <a class="nav-link" href="/">
                            Home <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="{{ Request::is('/about-us') ? 'nav-item active' : 'nav-item' }}">
                          <a class="nav-link" href="/about-us"> About </a>
                        </li>
                        <li class="{{ Request::is('/programms') ? 'nav-item active' : 'nav-item' }}">
                          <a class="nav-link" href="/programms"> Programs </a>
                        </li>
                        <li class="{{ Request::is('/contact-us') ? 'nav-item active' : 'nav-item' }}">
                          <a class="nav-link" href="/contact-us"> Contact us</a>
                        </li>
                        @guest
                            <li class="nav-item">
                              <a class="nav-link" href="{{ route('login') }}">Login</a>
                            </li>
                          @else  
                            <li class="nav-item">
                              <a class="nav-link" href="{{ route('home') }}">Print Certificate</a>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</header>