  <style type="text/css">
    form .error {
      color: #ff0000;
      margin-bottom: 7px;
    }
  </style>
  <section class="contact_section layout_padding">
    <div class="container ">
      <div class="heading_container ">
        <h2 class="">
          Request
          <span>
            A call Back
          </span>

        </h2>
      </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-md-6 ">
          <form method="post" id="contact_form" action="/contact-form">
            @csrf
            <div>
              <input type="text" name="name" placeholder="Name" />
            </div>
            <div>
              <input type="email" name="email" placeholder="Email" />
            </div>
            <div>
              <input type="text" name="phone" placeholder="Phone Number" />
            </div>
            <div>
              <input type="text" name="message" class="message-box" placeholder="Message" />
            </div>
            <div class="d-flex  mt-4 ">
              <button type="submit">
                SEND
              </button>
            </div>
          </form>
        </div>
        <div class="col-md-6">
          <!-- map section -->
          <div class="map_section">
            <div id="map" class="w-100 h-100"></div>
          </div>

          <!-- end map section -->
        </div>
      </div>
    </div>
  </section>
  <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
  <script>
    $("#contact_form").validate({
    // Specify validation rules
    rules: {
      // The key name on the left side is the name attribute
      // of an input field. Validation rules are defined
      // on the right side
      name: "required",
      email: {
        required: true,
        // Specify that email should be validated
        // by the built-in "email" rule
        email: true
      },
      phone: {
        required: true,
        minlength: 5,
        maxlength: 10
      },
      message: {
        required: true,
        maxlength: 500
      }
    },
    // Specify validation error messages
    messages: {
      name: "Please enter your name",
      phone: {
        required: "Please provide a phone no.",
        maxlength: "Your phone no. should be maximum 10 characters long"
      },
      email: "Please enter a valid email address",
      message: {
        required: "Please enter your message",
        maxlength: "Your message should not be more than 500 characters long"
      }
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
      form.submit();
    }
  });
    
  </script>