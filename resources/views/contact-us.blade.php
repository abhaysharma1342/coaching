@extends('layouts.master')
@section('content')

    <!-- header section strats -->

      @include('includes.header')

    <!-- end header section -->
    
  	<!-- contact section -->

    	@include('includes.contact-us')
      
  	<!-- end contact section -->

	<!-- footer section -->

		@include('includes.footer')
		
	<!-- footer section -->

@endsection	  	