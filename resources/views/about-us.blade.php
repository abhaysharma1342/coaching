@extends('layouts.master')
@section('content')

    <!-- header section starts -->

      @include('includes.header')

    <!-- end header section -->
    
  	<!-- about section -->

    	@include('includes.about-us')

  	<!-- end about section -->

		<!-- footer section -->

			@include('includes.footer')
			
		<!-- footer section -->

@endsection	  	