@extends('layouts.master')
@section('content')

    <!-- header section strats -->

      @include('includes.header')

    <!-- end header section -->

	<!-- offer section -->

	    @include('includes.courses')

	<!-- end offer section -->

		<!-- footer section -->

			@include('includes.footer')
			
		<!-- footer section -->

@endsection	  	