@extends('layouts.master')
@section('content')
  <div class="hero_area">

    <!-- header section strats -->

      @include('includes.header')

    <!-- end header section -->

    <!-- slider section -->

        @include('includes.slider')

    <!-- end slider section -->
  </div>

  <!-- offer section -->

      @include('includes.courses')

  <!-- end offer section -->

  <!-- about section -->

    @include('includes.about-us')

  <!-- end about section -->

  <!-- client section -->

      @include('includes.feedback')

  <!-- end client section -->

  <!-- contact section -->

      @include('includes.contact-us')
      
  <!-- end contact section -->

  <!-- footer section -->
  @include('includes.footer')
  <!-- footer section -->

@endsection