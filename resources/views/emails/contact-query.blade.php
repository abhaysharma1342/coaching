@component('mail::message')
<h3>Hi Admin,</h3>

		<p> You have got a Query From {{ $content['name'] }}</p>
		<p> {{ $content['message'] }} </p>
		<p> You can reach out the customer on Following Mail or Mobile no.</p>
		<p> {{ $content['email'] }}</p>
		<p> {{ $content['phone'] }}</p>

Thanks,<br>
{{ config('app.name') }}
@endcomponent
