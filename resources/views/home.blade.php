@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Ceritification Form</div>

                <div class="card-body">
                    <form method="post" action="print-certificate" autocomplete="off">
                        @csrf
                        <div class="form-group row">
                            <label for="colFormLabelLg" class="col-sm-4 col-form-label col-form-label-lg">Student name</label>
                            <div class="col-sm-8">
                              <input type="text" name="name" class="form-control form-control-lg" id="colFormLabelLg" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="colFormLabelLg" class="col-sm-4 col-form-label col-form-label-lg">Roll no.</label>
                            <div class="col-sm-8">
                              <input type="number" class="form-control form-control-lg" id="colFormLabelLg" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="colFormLabelLg" class="col-sm-4 col-form-label col-form-label-lg">Course</label>
                            <div class="col-sm-8">
                                <select class="custom-select mr-sm-2" name="course" id="inlineFormCustomSelect" required>
                                    <option selected disabled>Choose Course</option>
                                    <option value="Course1"> Course1 </option>
                                    <option value="Course2"> Course2 </option>
                                    <option value="Course3"> Course3 </option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="colFormLabelLg" class="col-sm-4 col-form-label col-form-label-lg">Duration</label>
                            <div class="col-sm-4">
                                <input type="number" name="duration" class="form-control form-control-lg" id="colFormLabelLg" min="1" required>
                            </div>
                            <div class="col-sm-4">
                                <select class="custom-select mr-sm-2" name="duration_type" id="inlineFormCustomSelect" required>
                                    <option selected disabled>Choose Type</option>
                                    <option value="Year"> Year </option>
                                    <option value="Month"> Month </option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12 offset-5">
                                <input type="reset" value="Reset" class="btn btn-warning"> &nbsp;
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
