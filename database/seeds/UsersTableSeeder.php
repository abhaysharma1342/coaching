<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name'              => 'Admin User',
                'email'             => 'admin123@yopmail.com',
                'password'          => \Hash::make(123456),
                'updated_at'        => now(),
                'created_at'        => now(),
            ]
        ]);
    }
}
